package deck

import (
	"fmt"
	"strconv"
)

type Card struct {
	/*
	Card:
	Struct that represents a card

	properties:
		Suit - string, one of {"Diamonds", "Hearts", "Spades", "Clubs"}
		Value - string, represents ordinal name of a card, line "7" or "King"
		Power - int, representation of card to make it possible to compare different cards
	*/
	Suit  string
	Value string
	Power int
}

func (c *Card) Init(suit, value string, intValue int) {
	c.Suit = suit
	c.Value = value
	c.Power = intValue
}

func (c *Card) Print() {
	fmt.Println(c.Suit, c.Value + ";", "Power: " + strconv.Itoa(c.Power))
}
