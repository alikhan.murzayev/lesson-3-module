package deck

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

type Deck struct {
	/*
	Deck:
	Represents a deck of cards that contains <= 36 cards.

	properties:
		Cards = slice on Card struct
	*/
	Cards []Card
}

func (d *Deck) AddCard(card *Card) {
	/*
	addCard:
	Adds a card into a deck if it has < 36 cards. Otherwise prints an error and returns.

	args:
		card - pointer to Card
	*/
	if len(d.Cards) >= 36 {
		fmt.Println("Error: deck if full.")
		return
	}
	d.Cards = append(d.Cards, *card)
}

func (d *Deck) Init() {
	/*
	Init:
	Fills in a deck with 36 cards.
	*/

	allSuits := [4]string{"Diamonds", "Hearts", "Spades", "Clubs"}
	allValues := [9]string{"6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"}

	for i, value := range allValues {
		for _, suit := range allSuits {
			card := Card{}
			card.Init(suit, value, i)
			d.AddCard(&card)
		}
	}
}

func (d *Deck) Shuffle() {
	/*
	Shuffle:
	Shuffles randomly a deck of 36 cards.
	*/

	rand.Seed(time.Now().UnixNano())
	for i := 0; i < len(d.Cards); i++ {
		swapIndex := rand.Int() % len(d.Cards)
		d.Cards[i], d.Cards[swapIndex] = d.Cards[swapIndex], d.Cards[i]
	}
}

func (d *Deck) Print() {
	if len(d.Cards) > 0 {
		for index, card := range d.Cards {
			fmt.Print("#", strconv.Itoa(index+1), ": ")
			card.Print()
		}
	} else {
		fmt.Println("Deck is empty.")
	}
}
