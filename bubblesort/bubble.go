package bubblesort

import (
	"fmt"
	"math/rand"
)


func BubbleSort(slice *[]int) {
	/*
	   bubbleSort:
	   Sort slice of integers in ascending order using Bubble Sort algorithm O(n^2).

	   args:
	   	slice - pointer to slice of integers
	*/

	for i := 0; i < len(*slice); i++ {
		for j := 0; j < len(*slice)-i-1; j++ {
			if (*slice)[j] > (*slice)[j+1] {
				(*slice)[j], (*slice)[j+1] = (*slice)[j+1], (*slice)[j]
			}
		}
	}
}

func QuickSort(slice *[]int, a, b int) {
	/*
	   quickSort
	   Sorts slice of integers in ascending order using Quick Sort algorithm O(n*log(n)).

	   args:
	   	slice - pointer to slice of integers
	   	a - start index
	   	b - end index
	*/

	// moving left and right positions
	l := a
	r := b
	// central element's index
	piv := (a + b) / 2

	for (*slice)[l] < (*slice)[piv] {
		l++
	}
	for (*slice)[r] > (*slice)[piv] {
		r--
	}
	if l <= r {
		(*slice)[l], (*slice)[r] = (*slice)[r], (*slice)[l]
		l++
		r--
	}
	if a < r {
		QuickSort(slice, a, r)
	}
	if b > l {
		QuickSort(slice, l, b)
	}
}

func GenerateRandomSlice(size, maxValue int) *[]int {
	/*
	   generateRandomSlice:
	   Generates a slice of integers between [0, maxValue].

	   args:
	   	size - length of the resultant slice

	   returns:
	   	pointer to a resultant slice
	*/

	var slice []int
	for i := 0; i < size; i++ {
		slice = append(slice, rand.Int()%maxValue+1)
	}
	return &slice
}

func printSlice(slice *[]int) {
	/*
	   printSlice:
	   Outputs a slice in one line into STDOUT.

	   args:
	   	slice - a pointer to a slice in integers
	*/

	for i := 0; i < len(*slice); i++ {
		fmt.Print((*slice)[i], " ")
	}
	fmt.Println()
}
